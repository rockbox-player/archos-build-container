# This file is a template, and might need editing before it works on your project.
FROM registry.gitlab.com/rockbox-player/base-build-container:master

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs. Or delete entirely if not needed.
RUN git clone https://gitlab.com/rockbox-player/rockbox.git
RUN cd rockbox
RUN ./tools/rockboxdev.sh s
RUN cd ..
RUN rm -rf rockbox
